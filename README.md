# Znalezione błędy. Nie działa zakład...
1. **bets/black z numerem 11**. Zakład powinien być wygrany, a nie jest.
2. **bets/red z numerem 12**. Zakład powinien być wygrany, a nie jest.
3. **bets/dozen/2 (numery od 13 do 24)**. Zakład powinien być wygrany, a nie jest.
4. **bets/dozen/3 (numery od 25 do 36)**. Zakład powinien być wygrany, a nie jest.
5. **bets/even z numerem 0**. Zakład powinien być przegrany, a nie jest.
6. **bets/high z numerem 19**. Zakład powinien być wygrany, a nie jest.
8. **bets/corner/31-32-34-35**. Api zwraca błąd z kodem 404.
9. **bets/line/ (wszystkie)**. Api zwraca błąd z kodem 404.
10. **bets/split/ (wszystkie)**. Współczynnik wypłaty powinien być 1:17, a jest 1:11.
11. **bets/straight/4**. Api zwraca błąd z kodem 404.