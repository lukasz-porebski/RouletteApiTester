﻿using FluentAssertions;
using NUnit.Framework;
using RouletteApiTester.Shared;
using System.Threading.Tasks;

namespace RouletteApiTester.Tests.ExternalBets
{
    public class PlayerTests : TestBase
    {
        [Test]
        public async Task Player_Should_Have100Chips()
        {
            var hashName = await GetHashName();
            var chips = await GetChips(hashName);

            chips.Should().Be(BaseChipsNumber);
        }
    }
}