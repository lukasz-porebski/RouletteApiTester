﻿using NUnit.Framework;
using RouletteApiTester.Shared;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteApiTester.Tests.ExternalBets
{
    public class LowHighTests : TestBase
    {
        private static IEnumerable<int> Lows => Pockets.Where(n => n > 0 && n < 19);
        private static IEnumerable<int> Highs => Pockets.Where(n => n >= 19);

        private const int Multiplier = 1 + 1;

        [TestCaseSource(nameof(Lows))]
        public async Task BetOnLows_Should_DoubleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/low",
                betPocket: number,
                multiplier: Multiplier);
        }

        [TestCaseSource(nameof(Highs))]
        public async Task BetOnHighs_Should_DoubleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/high",
                betPocket: number,
                multiplier: Multiplier);
        }
    }
}