﻿using NUnit.Framework;
using RouletteApiTester.Shared;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteApiTester.Tests.ExternalBets
{
    public class ColumnTests : TestBase
    {
        private static IEnumerable<int> First => GetColumn(1);
        private static IEnumerable<int> Second => GetColumn(2);
        private static IEnumerable<int> Third => GetColumn(3);

        private const int Multiplier = 1 + 2;

        [TestCaseSource(nameof(First))]
        public async Task BetOnFirstColumn_Should_TripleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/column/1",
                betPocket: number,
                multiplier: Multiplier);
        }

        [TestCaseSource(nameof(Second))]
        public async Task BetOnSecondColumn_Should_TripleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/column/2",
                betPocket: number,
                multiplier: Multiplier);
        }

        [TestCaseSource(nameof(Third))]
        public async Task BetOnThirdColumn_Should_TripleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/column/3",
                betPocket: number,
                multiplier: Multiplier);
        }

        private static IEnumerable<int> GetColumn(int start)
        {
            var pocket = start;
            var lastPocket = Pockets.Max();
            var result = new List<int>();

            while (pocket <= lastPocket)
            {
                result.Add(pocket);
                pocket += 3;
            }

            return result;
        }
    }
}