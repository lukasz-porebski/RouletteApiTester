﻿using NUnit.Framework;
using RouletteApiTester.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RouletteApiTester.Tests.ExternalBets
{
    public class ColorTests : TestBase
    {
        private static IEnumerable<int> Reds => new List<int>
        {
            1, 3, 5, 7, 9, 12, 14, 16, 18, 21, 23, 25, 27, 28, 30, 32, 34, 36
        };

        private static IEnumerable<int> Blacks => new List<int>
        {
            2, 4, 6, 8, 10, 11, 13, 15, 17, 19, 20, 22, 24, 26, 29, 31, 33, 35
        };

        private const int Multiplier = 1 + 1;

        [TestCaseSource(nameof(Reds))]
        public async Task BetOnReds_Should_DoubleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/red",
                betPocket: number,
                multiplier: Multiplier);
        }

        [Test]
        public async Task BetOnRedsEqualsZero_Should_Lose()
        {
            await AssertNumberOfChipsAfterBet(
                route: "bets/red",
                betPocket: 0,
                expectedNumberOfChips: 0);
        }

        [TestCaseSource(nameof(Blacks))]
        public async Task BetOnBlacks_Should_DoubleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/black",
                betPocket: number,
                multiplier: Multiplier);
        }

        [Test]
        public async Task BetOnBlacksEqualsZero_Should_Lose()
        {
            await AssertNumberOfChipsAfterBet(
                route: "bets/black",
                betPocket: 0,
                expectedNumberOfChips: 0);
        }
    }
}