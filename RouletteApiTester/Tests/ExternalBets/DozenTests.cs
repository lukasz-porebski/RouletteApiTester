﻿using NUnit.Framework;
using RouletteApiTester.Shared;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteApiTester.Tests.ExternalBets
{
    public class DozenTests : TestBase
    {
        private static IEnumerable<int> First => Pockets.Where(p => p >= 1 && p <= 12).ToList();
        private static IEnumerable<int> Second => Pockets.Where(p => p >= 13 && p <= 24).ToList();
        private static IEnumerable<int> Third => Pockets.Where(p => p >= 25 && p <= 36).ToList();

        private const int Multiplier = 1 + 2;

        [TestCaseSource(nameof(First))]
        public async Task BetOnFirstDozen_Should_TripleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/dozen/1",
                betPocket: number,
                multiplier: Multiplier);
        }

        [TestCaseSource(nameof(Second))]
        public async Task BetOnSecondDozen_Should_TripleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/dozen/2",
                betPocket: number,
                multiplier: Multiplier);
        }

        [TestCaseSource(nameof(Third))]
        public async Task BetOnThirdDozen_Should_TripleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/dozen/3",
                betPocket: number,
                multiplier: Multiplier);
        }
    }
}