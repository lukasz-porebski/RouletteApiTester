﻿using NUnit.Framework;
using RouletteApiTester.Shared;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteApiTester.Tests.ExternalBets
{
    public class EvenOddTests : TestBase
    {
        private static IEnumerable<int> Odds => Pockets.Where(n => n % 2 != 0);
        private static IEnumerable<int> EvensExceptZero => Pockets.Where(n => n != 0 && n % 2 == 0);

        private const int Multiplier = 1 + 1;

        [TestCaseSource(nameof(EvensExceptZero))]
        public async Task BetOnEvensExceptZero_Should_DoubleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/even",
                betPocket: number,
                multiplier: Multiplier);
        }

        [Test]
        public async Task BetOnEvensEqualsZero_Should_Lose()
        {
            await AssertNumberOfChipsAfterBet(
                route: "bets/even",
                betPocket: 0,
                expectedNumberOfChips: 0);
        }

        [TestCaseSource(nameof(Odds))]
        public async Task BetOnOdds_Should_DoubleNumberOfChips_If_GivenNumberIsSpinned(int number)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: "bets/odd",
                betPocket: number,
                multiplier: Multiplier);
        }
    }
}