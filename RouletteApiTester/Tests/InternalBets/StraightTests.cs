﻿using NUnit.Framework;
using RouletteApiTester.Shared;
using System.Threading.Tasks;

namespace RouletteApiTester.Tests.InternalBets
{
    public class StraightTests : TestBase
    {
        private const int Multiplier = 1 + 35;

        [TestCaseSource(nameof(Pockets))]
        public async Task BetOnStraight_Should_MultiplyBy36NumberOfChips_If_GivenNumberIsSpinned(int pocket)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: $"bets/straight/{pocket}",
                betPocket: pocket,
                multiplier: Multiplier);
        }
    }
}