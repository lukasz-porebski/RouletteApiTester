﻿using NUnit.Framework;
using RouletteApiTester.Shared;
using RouletteApiTester.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RouletteApiTester.Tests.InternalBets
{
    public class SplitTests : TestBase
    {
        private static IEnumerable<AdjacentPockets> AdjacentPockets => new List<AdjacentPockets>
        {
            new AdjacentPockets(new[] {0, 1}),
            new AdjacentPockets(new[] {1, 2}),
            new AdjacentPockets(new[] {1, 4}),
            new AdjacentPockets(new[] {2, 5}),
            new AdjacentPockets(new[] {3, 6}),
            new AdjacentPockets(new[] {2, 3}),
            new AdjacentPockets(new[] {4, 7}),
            new AdjacentPockets(new[] {5, 8}),
            new AdjacentPockets(new[] {6, 9}),
            new AdjacentPockets(new[] {3, 4}),
            new AdjacentPockets(new[] {7, 10}),
            new AdjacentPockets(new[] {8, 11}),
            new AdjacentPockets(new[] {9, 12}),
            new AdjacentPockets(new[] {4, 5}),
            new AdjacentPockets(new[] {10, 13}),
            new AdjacentPockets(new[] {11, 14}),
            new AdjacentPockets(new[] {12, 15}),
            new AdjacentPockets(new[] {5, 6}),
            new AdjacentPockets(new[] {13, 16}),
            new AdjacentPockets(new[] {14, 17}),
            new AdjacentPockets(new[] {15, 18}),
            new AdjacentPockets(new[] {6, 7}),
            new AdjacentPockets(new[] {16, 19}),
            new AdjacentPockets(new[] {17, 20}),
            new AdjacentPockets(new[] {18, 21}),
            new AdjacentPockets(new[] {7, 8}),
            new AdjacentPockets(new[] {19, 22}),
            new AdjacentPockets(new[] {20, 23}),
            new AdjacentPockets(new[] {21, 24}),
            new AdjacentPockets(new[] {8, 9}),
            new AdjacentPockets(new[] {22, 25}),
            new AdjacentPockets(new[] {23, 26}),
            new AdjacentPockets(new[] {24, 27}),
            new AdjacentPockets(new[] {9, 10}),
            new AdjacentPockets(new[] {25, 28}),
            new AdjacentPockets(new[] {26, 29}),
            new AdjacentPockets(new[] {27, 30}),
            new AdjacentPockets(new[] {10, 11}),
            new AdjacentPockets(new[] {28, 31}),
            new AdjacentPockets(new[] {29, 32}),
            new AdjacentPockets(new[] {30, 33}),
            new AdjacentPockets(new[] {11, 12}),
            new AdjacentPockets(new[] {31, 34}),
            new AdjacentPockets(new[] {32, 35}),
            new AdjacentPockets(new[] {33, 36}),
            new AdjacentPockets(new[] {12, 13}),
            new AdjacentPockets(new[] {14, 15}),
            new AdjacentPockets(new[] {15, 16}),
            new AdjacentPockets(new[] {16, 17}),
            new AdjacentPockets(new[] {17, 18}),
            new AdjacentPockets(new[] {18, 19}),
            new AdjacentPockets(new[] {19, 20}),
            new AdjacentPockets(new[] {20, 21}),
            new AdjacentPockets(new[] {21, 22}),
            new AdjacentPockets(new[] {22, 23}),
            new AdjacentPockets(new[] {23, 24}),
            new AdjacentPockets(new[] {24, 25}),
            new AdjacentPockets(new[] {25, 26}),
            new AdjacentPockets(new[] {26, 27}),
            new AdjacentPockets(new[] {27, 28}),
            new AdjacentPockets(new[] {28, 29}),
            new AdjacentPockets(new[] {29, 30}),
            new AdjacentPockets(new[] {30, 31}),
            new AdjacentPockets(new[] {30, 31}),
            new AdjacentPockets(new[] {31, 32}),
            new AdjacentPockets(new[] {32, 33}),
            new AdjacentPockets(new[] {33, 34}),
            new AdjacentPockets(new[] {34, 35}),
            new AdjacentPockets(new[] {35, 36}),
            new AdjacentPockets(new[] {0, 2}),
            new AdjacentPockets(new[] {0, 3}),
        };

        private const int Multiplier = 1 + 17;

        [TestCaseSource(nameof(GetTestParams))]
        public async Task BetOnSplit_Should_MultiplyBy18NumberOfChips_If_GivenNumberIsSpinned(string route, int pocket)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: $"bets/split/{route}",
                betPocket: pocket,
                multiplier: Multiplier);
        }

        private static List<object[]> GetTestParams() =>
            AdjacentPocketsToTestParams(AdjacentPockets);
    }
}