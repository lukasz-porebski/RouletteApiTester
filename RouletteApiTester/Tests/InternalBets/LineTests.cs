﻿using NUnit.Framework;
using RouletteApiTester.Shared;
using RouletteApiTester.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RouletteApiTester.Tests.InternalBets
{
    public class LineTests : TestBase
    {
        private static IEnumerable<AdjacentPockets> AdjacentPockets => new List<AdjacentPockets>
        {
            new AdjacentPockets(new[] {1, 2, 3, 4, 5, 6}),
            new AdjacentPockets(new[] {4, 5, 6, 7, 8, 9}),
            new AdjacentPockets(new[] {7, 8, 9, 10, 11, 12}),
            new AdjacentPockets(new[] {10, 11, 12, 13, 14, 15}),
            new AdjacentPockets(new[] {13, 14, 15, 16, 17, 18}),
            new AdjacentPockets(new[] {16, 17, 18, 19, 20, 21}),
            new AdjacentPockets(new[] {19, 20, 21, 22, 23, 24}),
            new AdjacentPockets(new[] {22, 23, 24, 25, 26, 27}),
            new AdjacentPockets(new[] {25, 26, 27, 28, 29, 30}),
            new AdjacentPockets(new[] {28, 29, 30, 31, 32, 33}),
            new AdjacentPockets(new[] {31, 32, 33, 34, 35, 36}),
        };

        private const int Multiplier = 1 + 5;

        [TestCaseSource(nameof(GetTestParams))]
        public async Task BetOnLine_Should_MultiplyBy6NumberOfChips_If_GivenNumberIsSpinned(string route, int pocket)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: $"bets/line/{route}",
                betPocket: pocket,
                multiplier: Multiplier);
        }

        private static List<object[]> GetTestParams() =>
            AdjacentPocketsToTestParams(AdjacentPockets);
    }
}