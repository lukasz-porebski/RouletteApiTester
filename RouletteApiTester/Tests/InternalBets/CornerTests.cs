﻿using NUnit.Framework;
using RouletteApiTester.Shared;
using RouletteApiTester.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RouletteApiTester.Tests.InternalBets
{
    public class CornerTests : TestBase
    {
        private static IEnumerable<AdjacentPockets> AdjacentPockets => new List<AdjacentPockets>
        {
            new AdjacentPockets(new[] {1, 2, 4, 5}),
            new AdjacentPockets(new[] {2, 3, 5, 6}),
            new AdjacentPockets(new[] {4, 5, 7, 8}),
            new AdjacentPockets(new[] {5, 6, 8, 9}),
            new AdjacentPockets(new[] {7, 8, 10, 11}),
            new AdjacentPockets(new[] {8, 9, 11, 12}),
            new AdjacentPockets(new[] {10, 11, 13, 14}),
            new AdjacentPockets(new[] {11, 12, 14, 15}),
            new AdjacentPockets(new[] {13, 14, 16, 17}),
            new AdjacentPockets(new[] {14, 15, 17, 18}),
            new AdjacentPockets(new[] {16, 17, 19, 20}),
            new AdjacentPockets(new[] {17, 18, 20, 21}),
            new AdjacentPockets(new[] {19, 20, 22, 23}),
            new AdjacentPockets(new[] {20, 21, 23, 24}),
            new AdjacentPockets(new[] {22, 23, 25, 26}),
            new AdjacentPockets(new[] {23, 24, 26, 27}),
            new AdjacentPockets(new[] {25, 26, 28, 29}),
            new AdjacentPockets(new[] {26, 27, 29, 30}),
            new AdjacentPockets(new[] {28, 29, 31, 32}),
            new AdjacentPockets(new[] {29, 30, 32, 33}),
            new AdjacentPockets(new[] {31, 32, 34, 35}),
            new AdjacentPockets(new[] {32, 33, 35, 36}),
            new AdjacentPockets(new[] {0, 1, 2, 3}),
        };

        private const int Multiplier = 1 + 8;

        [TestCaseSource(nameof(GetTestParams))]
        public async Task BetOnCorner_Should_MultiplyBy9NumberOfChips_If_GivenNumberIsSpinned(string route, int pocket)
        {
            await AssertMultiplicationNumberOfChipsAfterBet(
                route: $"bets/corner/{route}",
                betPocket: pocket,
                multiplier: Multiplier);
        }

        private static List<object[]> GetTestParams() =>
            AdjacentPocketsToTestParams(AdjacentPockets);
    }
}