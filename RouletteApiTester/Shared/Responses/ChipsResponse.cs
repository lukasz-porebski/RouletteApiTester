﻿namespace RouletteApiTester.Shared.Responses
{
    public class ChipsResponse
    {
        public int Chips { get; set; }
    }
}