﻿using System.Collections.Generic;
using System.Linq;

namespace RouletteApiTester.Shared.Models
{
    public readonly struct AdjacentPockets
    {
        public readonly IOrderedEnumerable<int> Pockets;

        public AdjacentPockets(IEnumerable<int> pockets)
        {
            Pockets = pockets.OrderBy(p => p);
        }

        public override string ToString() =>
            string.Join("-", Pockets);
    }
}