﻿using FluentAssertions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using RouletteApiTester.Shared.Exceptions;
using RouletteApiTester.Shared.Models;
using RouletteApiTester.Shared.Requests;
using RouletteApiTester.Shared.Responses;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteApiTester.Shared
{
    public abstract class TestBase
    {
        public static int BaseChipsNumber => 100;
        public static IEnumerable<int> Pockets => Enumerable.Range(0, 37);

        private const string ApiUrl = "https://roulette-api.nowakowski-arkadiusz.com";
        private readonly RestClient _restClient;

        protected TestBase()
        {
            _restClient = new RestClient(ApiUrl);
            var defaultSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            _restClient.UseNewtonsoftJson(defaultSettings);
        }

        protected async Task<string> GetHashName()
        {
            var player = await Post<PlayerResponse>("players");
            return player.HashName;
        }

        protected async Task<int> GetChips(string hashName)
        {
            var player = await Get<ChipsResponse>("chips", hashName);
            return player.Chips;
        }

        protected async Task Spin(string hashName, int number) =>
            await VoidPost($"spin/{number}", hashName);

        protected async Task AssertMultiplicationNumberOfChipsAfterBet(string route, int betPocket, int multiplier) =>
            await AssertNumberOfChipsAfterBet(route, betPocket, BaseChipsNumber * multiplier);

        protected async Task AssertNumberOfChipsAfterBet(string route, int betPocket, int expectedNumberOfChips)
        {
            var hashName = await GetHashName();
            await VoidPost(route, hashName, new ChipsRequest
            {
                Chips = BaseChipsNumber
            });
            await Spin(hashName, betPocket);
            var chips = await GetChips(hashName);

            chips.Should().Be(expectedNumberOfChips);
        }

        protected static List<object[]> AdjacentPocketsToTestParams(IEnumerable<AdjacentPockets> adjacentPockets)
        {
            var result = new List<object[]>();

            foreach (var ap in adjacentPockets)
                result.AddRange(ap.Pockets.Select(pocket => new object[]
                {
                    ap.ToString(), pocket
                }));

            return result;
        }

        protected async Task<T> Get<T>(string path, string hashName = "")
        {
            var request = CreateRequest(path, hashName);
            var response = await _restClient.ExecuteGetAsync<T>(request);

            if (response.IsSuccessful)
                return response.Data;

            throw new RequestException(response.StatusDescription);
        }

        protected async Task VoidPost(string path, string hashName = "", object body = default) =>
            await Post<Task>(path, hashName, body);

        protected async Task<T> Post<T>(string path, string hashName = "", object body = default)
        {
            var request = CreateRequest(path, hashName);
            request.AddJsonBody(body ?? "");
            var response = await _restClient.ExecutePostAsync<T>(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<T>(response.Content);

            throw new RequestException(response.StatusDescription);
        }

        private static RestRequest CreateRequest(string path, string hashName)
        {
            var request = new RestRequest(path, DataFormat.Json);
            request.AddHeader("Authorization", hashName);
            return request;
        }
    }
}