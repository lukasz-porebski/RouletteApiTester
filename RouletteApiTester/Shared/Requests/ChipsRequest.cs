﻿namespace RouletteApiTester.Shared.Requests
{
    public class ChipsRequest
    {
        public int Chips { get; set; }
    }
}